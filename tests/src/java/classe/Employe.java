/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classe;

import controller.ModelView;
import dao.GenericDAO;
import frame.AnnotMap;
import frame.Attribut;
import frame.Fonction;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author FITIA ARIVONY
 */
@AnnotMap(nomTable="emp")
public class Employe {
    @Attribut(attr="idemp",primary_key=true)
    Integer id;
    @Attribut(attr="nomemp")
    String nom;
    @Attribut(attr="prenomemp")
    String prenom;
    @Attribut(attr="addemp")
    String adresse;
    @Attribut(attr="daty")
    Date datenaissance;
    @Attribut(attr="sal")
    Double salaire;

    public Double getSalaire() {
        return salaire;
    }

    public void setSalaire(Double salaire) {
        this.salaire = salaire;
    }
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Date getDatenaissance() {
        return datenaissance;
    }

    public void setDatenaissance(Date datenaissance) {
        this.datenaissance = datenaissance;
    }
    @Fonction(url="listEmploye")
    public ModelView selectAll() throws SQLException{
        ModelView model=new ModelView();
        model.setUrl("lister.jsp");
        ArrayList<Object>obj=new ArrayList<>();
        obj.add("Rakoto Jean");
        obj.add("Ravelo Jeanne");
        model.getData().put("liste",obj);
        return model;   
    }
     @Fonction(url="tryy")
    public ModelView tryy()throws Exception{
       
        ModelView model=new ModelView();
       model.setUrl("filtrer.jsp");
       ArrayList<Object>objet=new ArrayList<>();
       objet.add(this);
         model.getData().put("liste",objet.toArray());
         return model; 
    }
    
    @Override
    public String toString() {
        String val="";
        val+="id="+this.getId()+"\n";
         val+="nom="+this.getNom()+"\n";
          val+="prenom="+this.getPrenom()+"\n";
           val+="adresse="+this.getAdresse()+"\n";
            val+="datenaissance="+this.getDatenaissance()+"\n";
            val+="salaire="+this.getSalaire()+"\n";
       return val;
    }
    
    
}
