<%-- 
    Document   : erreur
    Created on : 16 nov. 2022, 15:56:38
    Author     : FITIA ARIVONY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Error!</h1>
        <div>
            <% out.println((Exception)request.getAttribute("erreur"));   %>
        </div>
    </body>
</html>
